create database Student_manager;
use Student_manager;
drop database student_manager;
create table Class(
   Id_class int(11) primary key auto_increment,
   Name varchar(50) not null ,
   Id_teacher int(11) not null ,
   FOREIGN KEY (Id_teacher) REFERENCES Teacher(Id_teacher)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into Class(Name , Id_teacher) values("C1808I" , 1),
										   ("C1808L" , 2),
										   ("C1808G" , 3),
										   ("C1808H" , 4);

create table Teacher(
   Id_teacher int(11) primary key auto_increment,
   Name varchar(50) not null ,
   DOB varchar(50) not null ,
   Email varchar(50) not null
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into Teacher(Name , DOB , Email) values 
("Lê Việt Bách" , "12/3/1987"  , "bach@gmail.com"),
("Nguyễn Đức Hoàng" , "14/5/1989"  , "hoang@gmail.com"),
("Đỗ Quang Thơ" , "15/10/1985"  , "tho@gmail.com"),
("Nguyễn Ngọc Duy" , "20/8/1988"  , "duy@gmail.com");

drop  table Student;
create table Student(
   Id_student int(11) primary key auto_increment,
   Name varchar(50) not null ,
   Id_class int(11) not null ,
   DOB varchar(50) not null ,
   Address varchar(255) not null,
   Email varchar(50) not null,
   FOREIGN KEY (Id_class) REFERENCES Class(Id_class)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
Insert into Student(Name , Id_class  , DOB , Address , Email ) 
                          values("Linh" , 1 , "28/08/2000" ,"Ninh Bình" , "linh@gmail.com"),
                                 ("Hiếu", 2 , "29/10/2000" ,"Thái Bình" , "hieu@gmail.com"),
                                 ("Sơn" , 3 , "20/07/2000" ,"Bắc Ninh" , "son@gmail.com"),
                                 ("Mai" , 4 , "28/10/1998" ,"Bắc Giang" , "mai@gmail.com");
create table Mark(
Id_mark int(11) primary key auto_increment,
Mark double not null ,
Id_subject int(11) not null , 
Id_student int(11) not null,
FOREIGN KEY (Id_subject) REFERENCES Subject(Id_subject),
FOREIGN KEY (Id_student) REFERENCES Student(Id_student)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into Mark(Mark , Id_student , Id_subject) values(9 , 1 , 1 ),
													(8 , 2 , 2 ),
                                                    (9 , 3 , 3 ),
                                                    (9 , 4 , 4 );

create table Subject(
Id_subject int(11) primary key auto_increment,
Name varchar(50) not null 
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into Subject(name ) values("C"),
								("PHP" ),
								("HTML"),
								("Java" );

